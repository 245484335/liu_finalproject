﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    public int playerSpeed = 15;
    private bool facingRight = false;
    public int playerJumpPower = 1;
    private float moveX;
    public bool isGrounded;
    public float distanceToBottomOfPlayer = 0.9f;
    private Rigidbody2D rb;


    public void Start()
    {
        rb = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update () {
        PlayerMove();
        PlayerRaycast();

        //Core Kirby Mechnaic
        //if (other.CompareTag("Enemy1"))
        //{


        //}
    }

    void PlayerMove() {
        //Controls
        moveX = Input.GetAxis("Horizontal");
        if (Input.GetButtonDown("Jump")) {
            Jump();
        }
        //Animation
        //player direction
        if (moveX < 0.0f && facingRight == false)
        {
            FlipPlayer();
        }
        else if (moveX > 0.0f && facingRight == true) {
            FlipPlayer();
        }
        //Physics
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(moveX * playerSpeed, gameObject.GetComponent<Rigidbody2D>().velocity.y);
    }

    void Jump() {
        
        GetComponent<Rigidbody2D>().AddForce(Vector2.up * playerJumpPower);
    }



    void FlipPlayer() {
        facingRight = !facingRight;

        //Vector2 localScale = gameObject.transform.localScale;
        //localScale.x *= -1;
        //transform.localScale = localScale;
        transform.Rotate(0f, 180f, 0f);
    }

    /*void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Player has collided with" + col.collider.name);
    }*/

    void PlayerRaycast() {

        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down);
        if (hit != null && hit.collider != null && hit.distance < distanceToBottomOfPlayer && hit.collider.tag == "enemy")
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * 1000);
            //hit.collider.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.right * 200);
            //hit.collider.gameObject.GetComponent<BoxCollider2D>().gravity = 8;
            //hit.collider.gameObject.GetComponent<BoxCollider2D>().freezeRotation = false;
            //hit.collider.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            //hit.collider.gameObject.GetComponent<EnemyController>().enabled = false;
            Destroy(hit.collider.gameObject);


            if (hit != null && hit.collider != null && hit.distance < distanceToBottomOfPlayer && hit.collider.tag != "enemy")
            {
                isGrounded = true;
            }
        }



                //
                //Ray Up
                /*RaycastHit2D rayUp = Physics2D.Raycast(transform.position, Vector2.down);
                if (rayUp != null && rayUp.collider != null && rayUp.distance < distanceToBottomOfPlayer && rayUp.collider.tag == "MysteriousBox") {
                    Destroy(rayUp.collider.gameObject);
                }
                //Ray Down
                RaycastHit2D rayDown = Physics2D.Raycast(transform.position, Vector2.down);
                if (rayDown != null && rayDown.collider != null && rayDown.distance < distanceToBottomOfPlayer && rayDown.collider.tag == "enemy")
                {
                    GetComponent<Rigidbody2D>().AddForce(Vector2.up * 1000);
                    //hit.collider.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.right * 200);
                    //hit.collider.gameObject.GetComponent<BoxCollider2D>().gravity = 8;
                    //hit.collider.gameObject.GetComponent<BoxCollider2D>().freezeRotation = false;
                    //hit.collider.gameObject.GetComponent<BoxCollider2D>().enabled = false;
                    //hit.collider.gameObject.GetComponent<EnemyController>().enabled = false;
                    Destroy(rayDown.collider.gameObject);

                    if (rayDown != null && rayDown.collider != null && rayDown.distance < distanceToBottomOfPlayer && rayDown.collider.tag != "enemy") {
                        isGrounded = true;



                    }
                }*/
            }

   private void OnTriggerEnter2D (Collider2D other)
    {
        if (other.CompareTag("Ladder")) {
            rb.gravityScale = 0f;
            rb.velocity = new Vector2(rb.velocity.x, 0f);
        }        
    }

    private void OnTriggerExit2D (Collider2D other)
    {
        if (other.CompareTag("Ladder")) {
            rb.gravityScale = 10f;
        }
    }



    }
