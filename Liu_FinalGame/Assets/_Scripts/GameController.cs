﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public GameObject StoryCanvas;


    private void Awake()
    {
        Time.timeScale = 0f;
    }

    void Update () {
        if (Input.anyKeyDown)
        {
            Time.timeScale = 1f;
            StoryCanvas.SetActive(false);
        }
	}
}
