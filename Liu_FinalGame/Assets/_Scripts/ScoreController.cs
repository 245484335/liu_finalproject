﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour {
    
    public GameObject timeLeftUI;
    private float timeLeft = 300;



    /*public GameObject playerScoreUI;*/
    public Text playerScoreUI;
    public int playerScore;

    public PlayerHealth playerHealth;

    private void Start()
    {
        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();


        playerScore = 0;
        UpdateScore();

    }



    public void AddScore(int newScoreValue)
    {
        playerScore += newScoreValue;
        UpdateScore();
    }


    void UpdateScore() {
        playerScoreUI.text = "Score: " + playerScore;


        /*playerScoreUI.gameObject.GetComponent<Text>().text = ("Score: " + playerScore);*/
    }



    void CountScore()
    {
        playerScore = playerScore + (int)(timeLeft * 10);
    }

    void Update () {
        timeLeft -= Time.deltaTime;
        timeLeftUI.gameObject.GetComponent<Text>().text = ("Time Left: " + (int)timeLeft);
        
        if (timeLeft < 1) {
            SceneManager.LoadScene("MainScene1");
        }
	}

    void OnTriggerEnter2D(Collider2D trig)
    {
        if (trig.gameObject.name == "EndLevel") {
            CountScore();
        }
        /*if (trig.gameObject.name == "Coin") {
            playerScore += 10;
            Destroy(trig.gameObject);
        }*/
    }


    void TimesUp()
    {
        if (timeLeft < 0)
        {
            playerHealth.Die();
        }
    }


}
