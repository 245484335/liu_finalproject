﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact_Enemy : MonoBehaviour {

    public GameObject explosion;

    public int scoreValue;

    private ScoreController scoreController;


    public void Start()
    {
        scoreController = GameObject.FindGameObjectWithTag("Player").GetComponent<ScoreController>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Boundary" || other.tag == ("Enemy"))
        {
            return;
        }
            /*Instantiate(explosion, transform.position, transform.rotation);*/

        if (other.CompareTag("Bullet_Player"))
        {
            scoreController.AddScore(scoreValue);
            Destroy(other.gameObject);
            Destroy(gameObject);
        }

    }
}
