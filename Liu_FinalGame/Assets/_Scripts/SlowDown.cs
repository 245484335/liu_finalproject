﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowDown : MonoBehaviour
{

    /*public GameObject explosion;*/
    public PlayerController playerController;
    public PlayerHealth playerHealth;

    public void Start()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController> ();
        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth> ();

    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Boundary" || other.tag == "Enemy")
        {
            return;
        }
        /*Instantiate(explosion, transform.position, transform.rotation);*/

        if (other.tag == "Player")
        {
            /*Instantiate(playerexplosion, other.transform.position, other.transform.rotation);*/
            if (playerController.playerSpeed > 5) {
                playerController.playerSpeed -= 5;
                Destroy(gameObject);
            }
            else {
                Destroy(other.gameObject);
                Destroy(gameObject);
                playerHealth.Die();
                }
        }

    }

}
