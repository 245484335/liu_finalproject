﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_static : MonoBehaviour {
    public PlayerHealth playerHealth;


    public void Start()
    {
        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player") {
            Destroy(collision.gameObject);
            playerHealth.Die();
        }
    }

}
