﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour {


    public GameObject bulletPrefab1;
    public GameObject bulletPrefab2;
    public Transform firePoint;

    public float fireRate;


    private ScoreController scoreController;

    public void Start()
    {
        scoreController = GameObject.FindGameObjectWithTag("Player").GetComponent<ScoreController>();
    }


    void Update () {
        if (Input.GetButtonDown("Fire1") )
        {
            ShootDestroy();
        }

        if (scoreController.playerScore > 30 && Input.GetButtonDown("Fire2"))
        {
                Copy();
        }
        



    }

    void ShootDestroy()
    {
        Instantiate(bulletPrefab1, firePoint.position, firePoint.rotation);
    }


    void Copy() {
        Instantiate(bulletPrefab2, firePoint.position, firePoint.rotation);
    }


    /*void ShootSlowDown()
    {
        nextFire = Time.time + fireRate;
        Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
    }*/
}
