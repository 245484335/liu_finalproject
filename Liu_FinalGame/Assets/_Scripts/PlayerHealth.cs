﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour {
    /*public int health;*/
    public bool hasDied;

	// Use this for initialization
	public void Start () {
        hasDied = false;
	}
	
	// Update is called once per frame
	public void Update () {
        if (gameObject.transform.position.y < -18) {
            Die();
            //Debug.Log("Player Has Died");
        }
        /*if (health < 0){
            Die();
        }*/
	}

    public void Die() {
        SceneManager.LoadScene("MainScene1");
    }
}
